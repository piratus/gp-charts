import Highcharts from 'highcharts'
import SeriesLabel from 'highcharts/modules/series-label'
import StreamGraph from 'highcharts/modules/streamgraph'
import Theme from 'highcharts/themes/grid-light'

SeriesLabel(Highcharts)
StreamGraph(Highcharts)

Theme(Highcharts)

export function renderChart(container, options) {
  const { title, categories } = options;
  Highcharts.chart(container, {
    credits: {
      enabled: false,
    },

    title: {
      text: title,
      floating: true,
      align: 'bottom',
      margin: 100,
      x: 100,
      y: 100,
      style: {
        fontSize: 24,
      },
    },

    xAxis: {
      opposite: true,
      maxPadding: 0,
      type: 'category',
      categories,
      startOnTick: true,
      endOnTick: true,
      labels: {
        align: 'left',
        reserveSpace: true,
        rotation: 270,
        style: {
          fontSize: 14,
        },
      },
      lineWidth: 0,
      margin: 20,
      tickWidth: 0,
      tickmarkPlacement: 'on',
    },

    yAxis: {
      type: 'category',
      startOnTick: true,
      endOnTick: true,
      visible: false,
    },

    legend: {
      enabled: true,
    },

    ...options,
  })
}
