import { createAction, handleActions } from 'redux-actions'

export const setTableData = createAction('TABLE.SET_DATA',
  (name, rows) => ({name, rows}))

export default handleActions({
  [setTableData]: (state, {payload: {name, rows}}) =>
    ({...state, [name]: rows}),
}, {})
