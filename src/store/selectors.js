import filter from 'lodash-es/filter'
import first from 'lodash-es/first'
import property from 'lodash-es/property'
import { createSelector } from 'reselect'

export const tableRows = (tableName) =>
  createSelector(property('tables'), (data) => data[tableName] || [])

const tableData = (rows) => ({
  columns: Object.keys(first(rows) || {}),
  data: rows.map(Object.values),
})

export const getCountries = createSelector(tableRows('countries'), tableData)
export const getCircuits = createSelector(tableRows('circuits'), tableData)
export const getStageNames = createSelector(tableRows('stageNames'), tableData)

const _getRiders = createSelector(
  [
    tableRows('riders'),
    tableRows('results'),
  ],
  (riders, results) => riders.map((rider) => {
    const races = filter(results, {rider: rider.id})
    return ({
      ...rider,
      races: races.length,
      wins: filter(races, {position: 1}).length,
    })
  })
)

export const getRiders = createSelector(_getRiders, tableData)
