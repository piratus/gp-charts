import { combineReducers } from 'redux'

import tables from './modules/tables'

export default combineReducers({
  tables,
})
