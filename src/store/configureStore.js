import { createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import rootReducer from './rootReducer'

function configureStore(initialState = {}) {
  const store = createStore(rootReducer, initialState, composeWithDevTools())

  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      const { default: rootReducer } = require('./rootReducer')
      store.replaceReducer(rootReducer)
    })
  }

  return store
}

export default configureStore
