import { tsvParse } from 'd3-dsv'

export function loadTSV(name) {
  return Promise.resolve(loaders[name]())
    .then((module) => module.default ? module.default : module)
    .then((data) => tsvParse(data, parsers[name]))
}

function getDate(data, prop = 'date') {
  const [month, day] = data[prop].split('-')
  return new Date(Date.UTC(+data.year, month - 1, +day))
}

export function keyedBy(prop, valueProp = null) {
  return (data) => data.reduce((agg, item) =>
    Object.assign(agg, {[item[prop]]: valueProp ? item[valueProp] : item}), {})
}

export const loaders = {
  circuits: () => import(
    /* webpackChunkName: "data/circuits" */
    /* webpackMode: "lazy" */
    './data/circuits.tsv'),
  countries: () => import(
    /* webpackChunkName: "data/countries" */
    /* webpackMode: "lazy" */
    './data/countries.tsv'),
  gpCodes: () => import(
    /* webpackChunkName: "data/gp_country_codes" */
    /* webpackMode: "lazy" */
    './data/gp_country_codes.tsv'),
  stages: () => import(
    /* webpackChunkName: "data/stages" */
    /* webpackMode: "lazy" */
    './data/stages.tsv'),
  stageNames: () => import(
    /* webpackChunkName: "data/stage_names" */
    /* webpackMode: "lazy" */
    './data/stage_names.tsv'),
  winsByCountry: () => import(
    /* webpackChunkName: "data/wins_by_country" */
    /* webpackMode: "lazy" */
    './data/wins_by_country.tsv'),
  seasons: () => import(
    /* webpackChunkName: "data/seasons" */
    /* webpackMode: "lazy" */
    './data/seasons.json'),
  seasonResults: () => import(
    /* webpackChunkName: "data/season_results" */
    /* webpackMode: "lazy" */
    './data/season_results.tsv'),
  races: () => import(
    /* webpackChunkName: "data/races" */
    /* webpackMode: "lazy" */
    './data/races.tsv'),
  riders: () => import(
    /* webpackChunkName: "data/riders" */
    /* webpackMode: "lazy" */
    './data/riders.tsv'),
  results: () => import(
    /* webpackChunkName: "data/race_results" */
    /* webpackMode: "lazy" */
    './data/race_results.tsv'),
}

export const parsers = {
  circuits: (data) => data,
  countries: (data) => data,
  gpCodes: (data) => data,
  stages: (data) => ({
    ...data,
    year: +data.year,
    sequence: +data.sequence,
    date: getDate(data),
  }),
  stageNames: (data) => data,
  winsByCountry: (data) => ({
    ...data,
    year: +data.year,
    wins: +data.wins,
  }),
  seasons: (data) => data,
  seasonResults: (data) => ({
    ...data,
    year: +data.year,
    points: +data.points,
    position: +data.position,
  }),
  races: (data) => ({
    ...data,
    weather: weather(data.weather),
    year: +data.year,
    sequence: +data.sequence,
    air: data.air ? +data.air : null,
    humidity: data.humidity ? +data.humidity : null,
    ground: data.ground ? +data.ground : null,

  }),
  riders: (data) => data,
  results: (data) => ({
    ...data,
    year: +data.year,
    sequence: +data.sequence,
    position: +data.position,
    points: +data.points,
  }),
}

function weather(str) {
  switch (str) {
    case 'Clear': case 'Sunny':
      return 'Sunny/Clear'
    default:
      return str
  }
}
