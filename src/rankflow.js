import { tsvParse } from 'd3-dsv'
/* global zingchart */
import groupBy from 'lodash-es/groupBy'
import last from 'lodash-es/last'
import mapValues from 'lodash-es/mapValues'
import sortBy from 'lodash-es/sortBy'
import { loaders } from './data'

import './main.scss'

const YEAR = location.search && parseInt(location.search.slice(1)) || 2017

class Stage {
  constructor(data = {}) {
    data = {
      year: 0,
      sequence: '0',
      shortname: '',
      title: '',
      circuit: '',
      ...data,
    }

    Object.assign(this, {
      ...data,
      season: +data.year,
      sequence: +data.sequence,
      stage: data.sequence,
    })
  }
}

class SeasonResult {
  constructor(data = {}) {
    data = {
      'year': 0,
      'category': '',
      'id': '',
      'position': 0,
      'points': 0,
      ...data,
    }
    Object.assign(this, {
      ...data,
      id: data.rider,
      season: +data.year,
      position: +data.position,
      points: +data.points,
    })
  }
}

class RaceResult {
  constructor(data = {}) {
    data = {
      'year': 0,
      'stage': 0,
      'category': '',
      'id': '',
      'position': 30,
      'points': 0,
      'team': '',
      'bike': '',
      ...data,
    }
    Object.assign(this, {
      ...data,
      id: data.rider,
      season: +data.year,
      sequence: +data.sequence,
      stage: data.sequence,
      position: +data.position || 30,
      seasonPosition: +data.seasonPosition || 30,
      points: +data.points,
      seasonPoints: +data.seasonPoints,
    })
  }
}

Promise
  .all([
    Promise.resolve(loaders.stages())
      .then((data) => tsvParse(data, (row) => new Stage(row))),
    Promise.resolve(loaders.seasonResults())
      .then((data) => tsvParse(data, (row) => new SeasonResult(row))),
    Promise.resolve(loaders.results())
      .then((data) => tsvParse(data, (row) => new RaceResult(row))),
  ])
  .then(([stages, seasonResults, raceResults]) => ({
    stages: stages.filter((row) => row.season === YEAR),
    seasonResults: seasonResults.filter((row) => row.season === YEAR && row.category === 'MotoGP'),
    raceResults: raceResults.filter((row) => row.season === YEAR && row.category === 'MotoGP'),
  }))
  .then(({stages, seasonResults, raceResults}) => {
    let results = groupBy(raceResults, (r) => r.id)
    results = mapValues(results, (value) => {
      return stages
        .map((s) => value.find((v) => v.sequence === s.sequence) || new RaceResult())
        .reduce((agg, res) => [...agg, (last(agg) || 0) + res.points || 0], [])
    })

    const positions = stages.map((s, i) =>
      sortBy(Object.keys(results), (key) => -results[key][i])
    )

    return {
      'type': 'rankflow',
      'options': {
        'sep-space': 0,
      },
      'plotarea': {
        margin: 10,
      },

      'scale-x': {
        labels: [...stages.map((stage) => stage.shortname)],
        values: [...stages.map((stage) => stage.sequence)],
        item: {
          text: 'Rider',
        },
      },
      'series': seasonResults.map((row) => ({
        text: last(row.id.split('-')).toUpperCase(),
        rank: row.position,
        ranks: positions.map((standings) => standings.indexOf(row.id) + 1),
      })),
    }
  })
  .then((settings) => {
    zingchart.render({
      id: 'rankChart',
      data: settings,
      height: '100%',
      width: window.innerWidth, // 18 * 100 + 100,
    })
  })
