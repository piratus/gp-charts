import groupBy from 'lodash-es/groupBy'
import uniq from 'lodash-es/uniq'

export function render(renderChart, stages, stageNames, races) {
  const byYear = groupBy(stages, 'year')
  Object.values(byYear).forEach((value) => {
    const offset = Math.round(value.length / 2)
    value.forEach((stage) => {
      stage.position = +stage.sequence - offset
    })
  })

  const categories = uniq(stages.map((s) => s.year)).sort()
  const weathers = uniq(races.map(race => race.weather))

    // .map(([name, stages]) => ({
    //   name,
    //   data: stages.map(({year, position, shortname, date}) => ({
    //     y: position,
    //     x: +year,
    //     name: year,
    //     shortname,
    //     stage: stageNames[shortname].shortTitle,
    //     date,
    //   })),
    // }))

  renderChart('chart', {
    categories,
    series: weathers.map(weather => ({
      name: weather,
      data: categories.map(year => races.filter(r => r.year === year && r.weather === weather).length)
    })),

    title: 'Race conditions',

    chart: {
      type: 'column',
    },

    yAxis: {
      min: 0,
      stackLabels: {
        enabled: true,
      }
    },

    colors: [
      '#ffaf54',
        '#6e6cc2',
      '#11d493',
      '#114fd4',
      '#11a0d4',
      '#0b698b',
    ],

    tooltip: {
      shared: true,
      // headerFormat: '<b style="font-size: 18px">{point.key}</b><br>',
//       pointFormat:
//         `<span style="color:{point.color};">▇ </span>
// {point.date:%e %b} <b>{point.shortname}</b> {point.stage}<br>`,
    },

    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
        }
      }
    },
  })
}
