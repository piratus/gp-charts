import min from 'lodash-es/min'

import '../main.scss'

import { keyedBy, loadTSV } from '../data'
import { renderChart } from '../lib/highcharts'
import { render } from './chart'

Promise.all([
  loadTSV('stages'),
  loadTSV('stageNames').then(keyedBy('shortname')),
  loadTSV('races').then(races => races.filter(race => Boolean(race.weather) && race.category === 'MotoGP')),
]).then(([stages, stageNames, races]) => {
  console.log([stages, races])

  const minYear = min(races.map(race => race.year))
  stages = stages.filter(stage => stage.year >= minYear)

  render(renderChart, stages, stageNames, races)

  if (module.hot) {
    module.hot.accept('./chart', () => {
      const { render } = require('./chart')
      render(renderChart, stages, stageNames, races)
    })
  }
})


