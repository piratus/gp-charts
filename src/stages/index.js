import '../main.scss'

import { keyedBy, loadTSV } from '../data'
import { renderChart } from '../lib/highcharts'
import { render } from './chart'

Promise.all([
  loadTSV('stages'),
  loadTSV('stageNames').then(keyedBy('shortname')),
]).then(([stages, stageNames]) => {
  render(renderChart, stages, stageNames)

  if (module.hot) {
    module.hot.accept('./chart', () => {
      const { render } = require('./chart')
      render(renderChart, stages, stageNames)
    })
  }
})


