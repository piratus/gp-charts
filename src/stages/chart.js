import groupBy from 'lodash-es/groupBy'
import uniq from 'lodash-es/uniq'
import { COLORS } from '../constants'

export function render(renderChart, stages, stageNames) {
  const byYear = groupBy(stages, 'year')
  Object.values(byYear).forEach((value) => {
    const offset = Math.round(value.length / 2)
    value.forEach((stage) => {
      stage.position = +stage.sequence - offset
    })
  })

  const categories = uniq(stages.map((s) => +s.year))
  const series = Object.entries(groupBy(stages, 'shortname'))
    .map(([name, stages]) => ({
      name,
      data: stages.map(({year, position, shortname, date}) => ({
        y: position,
        x: +year,
        name: year,
        shortname,
        stage: stageNames[shortname].shortTitle,
        date,
      })),
    }))

  renderChart('chart', {
    categories,
    series,

    title: 'Grand Prix stages by year',

    chart: {
      type: 'spline',
    },

    colors: COLORS,

    tooltip: {
      shared: true,
      headerFormat: '<b style="font-size: 18px">{point.key}</b><br>',
      pointFormat:
        `<span style="color:{point.color};">▇ </span>
{point.date:%e %b} <b>{point.shortname}</b> {point.stage}<br>`,
    },

    plotOptions: {
      series: {
        connectNulls: false,
        crisp: true,
      },
      spline: {
        lineWidth: 0,
        states: {
          hover: {
            lineWidthPlus: 5,
          },
        },
        marker: {
          symbol: 'circle',
          lineColor: null,
          radius: 6,
          states: {
            hover: {
              radiusPlus: 6,
            },
          },
        },
      },
    },
  })
}
