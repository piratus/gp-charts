export const SERIES_MARKER = '▇'

export const CATEGORIES = [
  { name: 'MotoGP', order: 1, group: 'MotoGP/500cc' },
  { name: '500cc',  order: 2, group: 'MotoGP/500cc' },
  { name: '350cc',  order: 3, group: '350cc' },
  { name: 'Moto2',  order: 4, group: 'Moto2/250cc' },
  { name: '250cc',  order: 5, group: 'Moto2/250cc' },
  { name: 'Moto3',  order: 6, group: 'Moto3/125cc' },
  { name: '125cc',  order: 7, group: 'Moto3/125cc' },
  { name: '80cc',   order: 8, group: '80cc' },
  { name: '50cc',   order: 9, group: '50cc' },
]

export const COLORS = [
  '#11d493',
  '#d43b11',
  '#2ed411',
  '#ffaf54',
  '#1176d4',
  '#80d411',
  '#11c7d4',
  '#ffaf54',
  '#11d442',
  '#1128d4',
  '#11d4ba',
  '#d46211',
  '#11d41b',
  '#114fd4',
  '#7655bd',
  '#2ec9b1',
  '#59d411',
  '#11a0d4',
  '#8bbc21',
  '#2111d4',
]
