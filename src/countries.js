import get from 'lodash-es/get'
import groupBy from 'lodash-es/groupBy'
import keyBy from 'lodash-es/keyBy'
import mapValues from 'lodash-es/mapValues'

import { renderChart } from './lib/highcharts'
import { SERIES_MARKER } from './constants'
import { keyedBy, loadTSV } from './data'

import './main.scss'

const getCountries = () =>
  Promise.all([
    loadTSV('countries').then(keyedBy('code')),
    loadTSV('gpCodes').then(keyedBy('gpCode', 'isoCode')),
  ]).then(([countries, gpCodes]) => ({
    countries, gpCodes,
    get(isoCode) { return countries[isoCode] || null },
    getGP(gpCode) { return countries[gpCodes[gpCode]] || null },
  }))

Promise.all([
  loadTSV('winsByCountry'),
  getCountries(),
]).then(transformData).then(render)

function transformData([data, countryData]) {
  const byYear = mapValues(groupBy(data, 'year'), (i) => keyBy(i, 'country'))
  const years = Object.keys(byYear).map(Number).sort()

  const countries = data.reduce((agg, {country}) =>
    agg.includes(country) ? agg : agg.concat([country]), [])
    .map((country) => ({
      name: countryData.getGP(country).name,
      data: years.map((year) => +get(byYear, [year, country, 'wins'], 0)),
    }))

  return { categories: years, series: countries }
}

function render({categories, series}) {
  renderChart('chart', {
    series,
    categories,

    title: 'Grand Prix wins per country by year',

    chart: {
      type: 'streamgraph',
      zoomType: 'x',
    },

    tooltip: {
      shared: true,
      headerFormat: '<b style="font-size: 18px">{point.key}</b><br/>',
      pointFormatter() {
        return !this.y ? '' : (
          `<span style="color:${this.color}">${SERIES_MARKER}</span>
           ${this.series.name}: <b>${this.y}</b><br/>`
        )
      },
    },

    plotOptions: {
      streamgraph: {
        dataLabels: {
          allowOverlap: true,
          padding: 0,
        },
        trackByArea: true,
      },
      series: {
        label: {
          minFontSize: 5,
          maxFontSize: 32,
          style: {
            color: 'rgba(255,255,255,1)',
            textShadow: '0 0 15px rgba(96,96,96,1)',
          },
        },
      },
    },
  })
}
