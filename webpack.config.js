/* eslint-env node */
const path = require('path')
const Autoprefixer = require('autoprefixer')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

const DEV = process.env.NODE_ENV !== 'production'

const PUBLIC_PATH = process.env.PUBLIC_PATH ||
  (DEV ? 'http://localhost:8080/' : '/')

const SOURCE_MAP = process.env.SOURCE_MAP ||
  (DEV ? 'cheap-module-eval-source-map' : false)

const AUTOPREFIXER = {
  browsers: [
    'Chrome 60',
    'Safari 10',
    'IE 11',
  ],
}

const HTML_PLUGIN = {
  favicon: 'public/favicon.ico',
  minify: {
    collapseWhitespace: !DEV,
    minifyJS: !DEV,
  },
}

const config = {
  mode: DEV ? 'development' : 'production',
  devtool: SOURCE_MAP,

  entry: {
    'countries': './src/countries.js',
    'rankflow': './src/rankflow.js',
    'stages': './src/stages/index.js',
    'races': './src/races/index.js',
  },

  output: {
    filename: DEV ? '[name].js' : '[name].[chunkhash].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: PUBLIC_PATH,
    pathinfo: DEV,
  },

  module: {
    wrappedContextCritical: true,
    rules: [
      {
        test: /\.jsx?$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: path.resolve('node_modules/.cache'),
          },
        },
        include: [
          path.resolve('src'),
        ],
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              sourceMap: Boolean(SOURCE_MAP),
              importLoaders: 1,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: Boolean(SOURCE_MAP),
              plugins: () => [
                Autoprefixer(AUTOPREFIXER),
              ],
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: Boolean(SOURCE_MAP),
            },
          },
        ],
      },
      {
        test: /\.tsv$/,
        use: 'tsv-loader',
      },
    ],
  },

  resolveLoader: {
    modules: [
      'node_modules',
      path.resolve('./build'),
    ],
  },

  plugins: [
    new HtmlWebpackPlugin(Object.assign({}, HTML_PLUGIN, {
      title: 'GP Charts',
      template: 'templates/index.html',
      chunks: [],
    })),

    new HtmlWebpackPlugin(Object.assign({}, HTML_PLUGIN, {
      title: 'Rankflow — GP Charts',
      template: 'templates/rankflow.html',
      filename: 'rankflow.html',
      chunks: ['manifest', 'common', 'rankflow'],
    })),

    new HtmlWebpackPlugin(Object.assign({}, HTML_PLUGIN, {
      title: 'Winners by Country — GP Charts',
      template: 'templates/chart.ejs',
      filename: 'countries.html',
      chunks: ['manifest', 'common', 'countries'],
    })),

    new HtmlWebpackPlugin(Object.assign({}, HTML_PLUGIN, {
      title: 'Stages by Year — GP Charts',
      template: 'templates/chart.ejs',
      filename: 'stages.html',
      chunks: ['manifest', 'common', 'stages'],
    })),

    new HtmlWebpackPlugin(Object.assign({}, HTML_PLUGIN, {
      title: 'Race conditions by year — GP Charts',
      template: 'templates/chart.ejs',
      filename: 'races.html',
      chunks: ['manifest', 'common', 'races'],
    })),

    new webpack.LoaderOptionsPlugin({
      minimize: !DEV,
      debug: DEV,
    }),
  ],

  performance: DEV ? false : {
    hints: 'warning',
  },

  stats: {
    assetsSort: 'name',
  },

  devServer: {
    contentBase: './public',
    watchContentBase: true,
    historyApiFallback: true,
    stats: {
      assetsSort: 'name',
      excludeAssets: /\.map$/,
      modules: false,
      cached: false,
      chunks: false,
      emitted: false,
      children: false,
      maxModules: Infinity,
      optimizationBailout: true,
    },
  },
}

if (DEV) {
  config.plugins.push(

  )
}

if (!DEV) {
  config.plugins.push(
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
  )
}

module.exports = config
